"use strict";

import { createElt, createImg } from "./tool.js";

// Get local storage
let ls = JSON.parse(localStorage.getItem("bre"));

// Init local storage if it doesn't exists
if (ls === null) {
    ls = {
        "actualPlayer": 1,
        "selectedPiece": null,
        "selectedPlace": null,
        "chessboard":  generateChessboard(7)
    };
    localStorage.setItem("bre", JSON.stringify(ls));
}

let pieces = document.getElementsByClassName("piece");

for (const piece of pieces) {
    if (ls["selectedPiece"] === piece.id) {
        piece.classList.add("selectedPiece");
    }
    piece.addEventListener("click", () => {
        let selected = document.getElementsByClassName("selectedPiece");
        // Delete style from previous selected
        if (selected.length > 0) selected[0].classList.remove("selectedPiece");
        if (ls["selectedPiece"] === piece.id) {
            updateLocalStorage("selectedPiece", null);
        } else {
            updateLocalStorage("selectedPiece", piece.id);
            // Add selected class
            piece.classList.add("selectedPiece");
        }
    });
}

function generateChessboard(size) {
    // Size must be odd
    if (size % 2 === 0) size++;
    let array = [];
    for (let i = 0; i < size; i++) {
        let row = [];
        for (let j = 0; j < size; j++) {
            row.push(0);
        }
        array.push(row);
    }
    return array;
}

function displayChessboard(chessboard) {
    let chessboardSection = document.getElementById("chessboard");
    for (let i = 0; i < chessboard.length; i++) {
        let row = createElt("div", null, null, "row");
        for (let j = 0; j < chessboard.length; j++) {
            let cell = createElt("div", null, `${i}${j}`, "cell");
            if (ls["selectedPlace"] === `${i}${j}`) {
                cell.classList.add("selectedPlace");
            }
            cell.addEventListener("click", () => {
                // console.log(`${i}:${j}`);
                let selected = document.getElementsByClassName("selectedPlace");
                // Delete style from previous selected 
                if (selected.length > 0) selected[0].classList.remove("selectedPlace");
                if (ls["selectedPlace"] === `${i}${j}`) {
                    updateLocalStorage("selectedPlace", null);
                } else {
                    updateLocalStorage("selectedPlace", `${i}${j}`);
                    // Add selected class
                    cell.classList.add("selectedPlace");
                }
            })
            row.appendChild(cell);
        }
        chessboardSection.appendChild(row);
    }
}

function placePiece(x, y, type) {
    console.log(`${x}:${y} => ${type}`);
    let cell = document.getElementById(`${x}${y}`);
    cell.appendChild(createImg(type));
}

function updateLocalStorage(key, value) {
    ls[key] = value;
    localStorage.setItem("bre", JSON.stringify(ls));
}

displayChessboard(ls["chessboard"]);

let btnPlace = document.getElementById("place");

btnPlace.addEventListener("click", () => {
    if (ls["selectedPlace"] !== null && ls["selectedPiece"] !== null) {
        placePiece(parseInt(ls["selectedPlace"][0]), parseInt(ls["selectedPlace"][1]), ls["selectedPiece"]);
    } else {
        console.log("error");
    }
})

let btnReset = document.getElementById("reset");

btnReset.addEventListener("click", () => {
    let selectedPlace = document.getElementsByClassName("selectedPlace");
    if (selectedPlace.length > 0) selectedPlace[0].classList.remove("selectedPlace");
    let selectedPiece = document.getElementsByClassName("selectedPiece");
    if (selectedPiece.length > 0) selectedPiece[0].classList.remove("selectedPiece");

    ls = {
        "actualPlayer": 1,
        "selectedPiece": null,
        "selectedPlace": null,
        "chessboard":  generateChessboard(7)
    };
    localStorage.setItem("bre", JSON.stringify(ls));
})